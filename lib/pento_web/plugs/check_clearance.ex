defmodule PentoWeb.Plug.CheckClearance do
 import Plug.Conn
 import Phoenix.Controller

 alias PentoWeb.Router.Helpers, as: Routes

 def init(opts), do: Enum.into(opts, %{})

 ## default opts // [] removed due to warning from dialyzer
 def call(conn, opts) do
   check_clearance(conn, opts)
 end

 defp check_clearance(conn, opts) do
   admin_level = conn.assigns.clearance_level
   can?(conn, admin_level, opts.clearance_level)
 end

 defp can?(conn, admin_level, clearance_level) when admin_level >= clearance_level, do: conn

 defp can?(conn, _user_level, _clearance_level), do: halt_plug(conn)


 defp halt_plug(conn) do
   conn
   |> redirect(to: Routes.page_path(conn, :index))
   |> halt()
 end

end
