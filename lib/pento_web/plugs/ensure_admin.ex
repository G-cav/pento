defmodule PentoWeb.Plug.EnsureAdmin do
 import Plug.Conn
 import Phoenix.Controller

 alias Pento.Admin
 alias PentoWeb.Router.Helpers, as: Routes

 def init(opts), do: Enum.into(opts, %{})

 def call(conn, opts \\ []) do
   check_permissions(conn, opts)
 end

 defp check_permissions(conn, _opts) do
   current_user = conn.assigns.current_user
   case Admin.get_permission_by_user(current_user) do
    nil ->
     halt_plug(conn)
    clearance_level ->
     assign(conn, :clearance_level, clearance_level)
   end
 end

 defp halt_plug(conn) do
   conn
   |> assign(:clearance_level, nil)
   |> redirect(to: Routes.page_path(conn, :index))
   |> halt()
 end

end
