defmodule PentoWeb.PromoLive do
  use PentoWeb, :live_view

  alias Pento.Promo
  alias Pento.Promo.Recipient

  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign_recipient()
     |> assign_changeset()}
  end

  defp assign_recipient(socket) do
    socket
    |> assign(:recipient, %Recipient{})
  end

  defp assign_changeset(%{assigns: %{recipient: recipient}} = socket) do
    socket
    |> assign(:changeset, Promo.change_recipient(recipient))
  end

  def handle_event(
        "validate",
        %{"recipient" => params},
        %{assigns: %{recipient: recipient}} = socket
      ) do
    changeset =
      recipient
      |> Promo.change_recipient(params)
      |> Map.put(:action, :validate)

    {:noreply,
     socket
     |> assign(:changeset, changeset)}
  end

  def handle_event(
        "save",
        %{"recipient" => params},
        %{assigns: %{recipient: recipient}} = socket
      ) do
    case Promo.send_promo(recipient, params) do
      {:ok, :sent} ->
        {:noreply,
         socket
         |> put_flash(:info, "Email to #{Map.get(params, "email")} has been sent")}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply,
         assign(
           socket
           |> put_flash(:error, "Could not send email, see form below"),
           changeset: changeset
         )}
    end
  end
end
