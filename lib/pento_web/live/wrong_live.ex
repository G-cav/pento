defmodule PentoWeb.WrongLive do
  use PentoWeb, :live_view

  
  def mount(_params, session, socket) do
    {
      :ok,
      assign(
        socket,
        score: 0,
        answer: new_game(),
        message: "Guess a number.",
        win: false,
        session_id: session["live_socket_id"]
      )
    }
  end

  def handle_event("guess", %{"number" => guess}, socket) do
    IO.inspect(socket.assigns.answer)
    IO.inspect(socket.assigns.session_id)
    {win, score, message} =
      cond do
        String.to_integer(guess) == socket.assigns.answer ->
          {true, socket.assigns.score + 1, "Correct"}

        true ->
          {false, socket.assigns.score - 1, "Your guess: #{guess}. Wrong. Guess again."}
      end

    {
      :noreply,
      assign(
        socket,
        message: message,
        score: score,
        win: win
      )
    }
  end

  # def handle_event("reset", %{}, socket) do
  #  Routes.live_path()
  # end
  def time() do
    DateTime.utc_now()
    |> to_string
  end

  def new_game(), do: :rand.uniform(10)

  def handle_params(%{"score" => score}, _uri, socket) do
    {:noreply,
     assign(socket,
       score: String.to_integer(score),
       win: false,
       answer: new_game(),
       message: "Guess a number."
     )}
  end

  def handle_params(params, _uri, socket) do
    IO.inspect(params)
    {:noreply, socket}
  end

end
