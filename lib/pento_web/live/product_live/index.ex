defmodule PentoWeb.ProductLive.Index do
  use PentoWeb, :live_view

  alias Pento.Catelog
  alias Pento.Catelog.Product

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
    socket
    |> assign(:greeting, "Welcome to Pento!")
    |> assign(:products, list_products())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    IO.inspect(socket)
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Product")
    |> assign(:product, Catelog.get_product!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Product")
    |> assign(:product, %Product{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Products")
    |> assign(:product, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    product = Catelog.get_product!(id)
    {:ok, _} = Catelog.delete_product(product)

    {:noreply, assign(socket, :products, list_products())}
  end

  defp list_products do
    Catelog.list_products()
  end
end
