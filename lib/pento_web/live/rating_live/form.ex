defmodule PentoWeb.RatingLive.Form do
 use PentoWeb, :live_component
 alias Pento.Survey
 alias Pento.Survey.Rating

 def update(assigns, socket) do
   {:ok,
    socket
    |> assign(assigns)
    |> assign_rating()
    |> assign_changeset()}
 end

 defp assign_rating(%{assigns: %{current_user: current_user, product: product}} = socket) do
  assign(socket, :rating, %Rating{user_id: current_user.id, product_id: product.id})
 end

 defp assign_changeset(%{assigns: %{rating: rating}} = socket) do
   assign(socket, :changeset, Survey.change_rating(rating))
 end

 def handle_event("save", %{"rating" => params}, socket) do
   {:noreply, save_rating(socket, params)}
 end

 def handle_event("validate", %{"rating" => params}, socket) do
  {:noreply, validate_rating(socket, params)}
 end

 defp save_rating(%{assigns: %{product_index: product_index, product: product}} = socket, params) do
   case Survey.create_rating(params) do
     {:ok, rating} ->
      product = %{product | ratings: [rating]}
      send(self(), {:created_rating, product, product_index})
      socket
     {:error, %Ecto.Changeset{} = changeset} ->
      assign(socket, changeset: changeset)
   end
 end



 defp validate_rating(socket, params) do
   changeset =
    socket.assigns.rating
    |> Survey.change_rating(params)
    |> Map.put(:action, :validate)

   assign(socket, :changeset, changeset)
 end
end
