defmodule PentoWeb.UserAuthLive do
 import Phoenix.LiveView
 alias Pento.Accounts
 alias Pento.Admin
 def on_mount(_, _params, %{"user_token" => user_token = _session}, socket) do
   socket = assign_new(socket, :current_user, fn ->
      Accounts.get_user_by_session_token(user_token)
   end)

   if socket.assigns.current_user do
    {:cont,
    socket
    |> assign(:clearance_level, Admin.get_permission_by_user(socket.assigns.current_user))}
   else
    {:halt, redirect(socket, to: "/login")}
   end
 end
end
