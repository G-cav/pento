defmodule PentoWeb.PageController do
  use PentoWeb, :controller

  alias Pento.Admin
  def index(conn, _params) do
    Admin.get_permission_by_user(conn.assigns.current_user)
      |>IO.inspect()
    render(conn, "index.html")
  end
end
