defmodule Pento.Promo do
 alias Pento.Promo.Recipient
 @moduledoc """
  Context for Promo
 """

 @spec change_recipient(Recipient.t(), %{email: String.t(), first_name: String.t()} | map()) :: Ecto.Changeset.t()

@doc """
 creates a schemaless changeset that checks for a valid name and email
"""
 def change_recipient(%Recipient{} = recipient, attrs \\ %{}) do
  Recipient.changeset(recipient, attrs)
 end


@spec send_promo(Recipient.t(), map()) :: {:ok, :sent} | {:error, Ecto.Changeset.t()}
 @doc """
   Stub for sending promo
 """
 def send_promo(recipient, params) do
   # send email to promo recipient
   changeset =
    recipient
    |> Recipient.changeset(params)
    
   case changeset.valid? do
    :true -> {:ok, :sent}
    :false -> {:error, changeset}
   end
 end
end
