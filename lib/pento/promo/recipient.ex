defmodule Pento.Promo.Recipient do
  defstruct [:first_name, :email]
  @types %{first_name: :string, email: :string}

  alias Pento.Promo.Recipient
  alias EctoCommons.EmailValidator
  import Ecto.Changeset

  @type t :: %__MODULE__{first_name: String.t(), email: String.t()}
  
  def changeset(%Recipient{} = user, attrs) do
    {user, @types}
    |> cast(attrs, Map.keys(@types))
    |> validate_required([:first_name, :email])
    |> EmailValidator.validate_email(:email)
  end
end
