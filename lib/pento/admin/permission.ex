defmodule Pento.Admin.Permission do
  use Ecto.Schema
  import Ecto.Changeset
  alias Pento.Accounts.User

  schema "permissions" do
    field :clearance_level, :integer
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(role, attrs) do
    role
    |> cast(attrs, [:clearance_level, :user_id])
    |> validate_required([:clearance_level, :user_id])
    |> validate_number(:clearance_level, greater_than: 0, less_than: 4, message: "Must be between 1 and 3")
  end
end
