defmodule Pento.AdminFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Pento.Admin` context.
  """

  @doc """
  Generate a permission.
  """
  def permission_fixture(attrs \\ %{}) do
    {:ok, permission} =
      attrs
      |> Pento.Admin.create_permission()

    permission
  end
end
