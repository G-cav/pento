defmodule Pento.Repo.Migrations.CreatePermissions do
  use Ecto.Migration

  def change do
    create table(:permissions) do
      add :clearance_level, :integer
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:permissions, [:user_id])
  end
end
